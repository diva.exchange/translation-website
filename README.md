# How to translate the diva.exchange website

Thank you very much for considering your contribution! Translations are important.

Please follow these steps: 

1. Please let us know what language you can contribute, and what is your origin language. 

For example: I want to contribute "Chinese" and the origin language is "German".

2. We create three Rich Text templates on cryptpad where you can add the translation of the new language based on the origin language text. One template for "all site texts", one template for "all blog posts" and one template for "all meta content". 

3. We send you three links to the Rich Text templates on cryptpad. For example: 

* Link to the template "all site texts in chinese language":  https://cryptpad.fr/pad/#/2/pad/edit/6yyki9KZTbPOxZPlxm+3SG3-/
* Link to the template "all blog post texts in chinese language": https://cryptpad.fr/pad/#/2/pad/edit/FHAn7hzakDz+5+sAJz2IsXJ0/
* Link to the template "all meta content in all languages":  https://cryptpad.fr/pad/#/2/pad/edit/YtQfj05jcaz9jKzSgFIEnymW/

4. You click on each link and you save the Rich Text templates in your cryptpad account

5. You edit the Richt Text templates and you add the translation after each section below the text of the origin language

6. When you are done: please let us know

7. We check the translation and get back to you. In case of questions we add comments directly into the Rich Text templates on cryptpad. We discuss and solve the question with you. 

8. We add the new language to wordpress and we inform you when the new language is live. If you could do a final review on the diva.exchange website, that would be great.  

Please get in contact with us on one of these channels:

Telegram: https://t.me/diva_exchange_chat_de
Email: carolyn@diva.exchange
Mastodon: https://social.diva.exchange/@social


YOU ARE A SUPERSTAR - THANK YOU!

Any feedback is welcome! 

